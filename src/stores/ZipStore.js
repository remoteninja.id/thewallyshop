import {observable, decorate, action} from 'mobx'
import { GET_ZIP_CODES,API_SUBSCRIBE_EMAIL } from '../config'

let index = 0

class ZipStore {
  zipcodes = []
  selectedZip = null

  async loadZipCodes() {
    const resp = await fetch(GET_ZIP_CODES).then((res) => res.json())
    this.zipcodes = resp
  }

  validateZipCode(zip) {
    const valid = this.zipcodes.find((z) => z == zip)
    if (valid) return true
    return false
  }

  async subscribe(email) {
    return fetch(API_SUBSCRIBE_EMAIL)
      .then(res => res.json())
  }
}

decorate(ZipStore, {
  zipcodes: observable,
  selectedZip: observable,
  loadZipCodes: action,
  subsribe: action,
})


export default new ZipStore()
