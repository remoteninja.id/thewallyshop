//w,export const BASE_URL = "http://api.tws.rizkifuad.com"
export const BASE_URL = "https://the-wally-shop.herokuapp.com"

export const API_GET_USER = BASE_URL + "/api/user"
export const API_EDIT_USER = BASE_URL + "/api/user"
export const API_SIGNUP= BASE_URL + "/api/signup"
export const API_LOGIN = BASE_URL + "/api/login"
export const API_FORGOT_PASSWORD = BASE_URL + "/api/user/forgot_password"
export const API_UPDATE_PASSWORD = BASE_URL + "/api/user/update_password"
export const API_GET_LOGIN_STATUS = BASE_URL + "/api/login/status"
export const API_REFER_FRIEND = BASE_URL + "/api/user/refer"

export const API_ADDRESS_NEW = BASE_URL + "/api/user/address"
export const API_ADDRESS_EDIT = BASE_URL + "/api/user/address"
export const API_ADDRESS_REMOVE = BASE_URL + "/api/user/address/"

export const API_PAYMENT_NEW = BASE_URL + "/api/user/payment"
export const API_PAYMENT_EDIT = BASE_URL + "/api/user/payment"
export const API_PAYMENT_REMOVE = BASE_URL + "/api/user/payment/"

export const API_GET_PRODUCT_DISPLAYED = BASE_URL + "/api/products/"
export const API_GET_PRODUCT_DETAIL = BASE_URL + "/api/product/"

export const API_GET_CATEGORIES = BASE_URL + "/api/categories"
export const API_SEARCH_KEYWORD = BASE_URL + "/api/products/search/"
export const API_REFRESH_INVENTORY = BASE_URL + "/api/products/refresh"


export const API_GET_CURRENT_CART = BASE_URL + "/api/cart"
export const API_EDIT_CURRENT_CART = BASE_URL + "/api/cart/edit"

export const API_CREATE_ORDER = BASE_URL + "/api/order/new"
export const API_GET_ORDER_SUMMARY = BASE_URL + "/api/order/"
export const API_UPDATE_ORDER = BASE_URL + "/api/order/update"
export const API_CHECK_PROMO = BASE_URL + "/api/check_promo"

export const API_GET_ADVERTISEMENTS = BASE_URL + "/api/get_advertisements"
export const GET_ZIP_CODES = BASE_URL + "/api/service/zipcodes"

export const API_HELP_GET_QUESTION = BASE_URL + "/api/help/questions"
export const API_HELP_GET_QUESTION_SINGLE = BASE_URL + "/api/help/question"
export const API_HELP_GET_HELP_TOPICS = BASE_URL + "/api/help/gethelptopics"
export const API_HELP_GET_CONTACT = BASE_URL + "/api/help/getcontact"

export const API_SUBSCRIBE_EMAIL = BASE_URL + "/api/email/signup"

export const INSTAGRAM = "http://instagram.com"
export const FACEBOOK = "http://facebook.com"

export const GOOGLE_API_KEY="AIzaSyB2IlvjI5MCPefFjne3cgbklq_1899QiXU"

export const APP_URL = "http://rizkifuad.com:3000"
